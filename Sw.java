import java.net.URL;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import gnu.io.CommPortIdentifier; 
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent; 
import gnu.io.SerialPortEventListener; 
import java.util.Enumeration;


class Sw implements SerialPortEventListener {
	private static URL url;
	private SerialPort serialPort;
	private BufferedReader input;
	private OutputStream output;
	private static final int DATA_RATE = 9600;
	private static final int TIME_OUT = 2000;
	private static String currentState = "off";
	private CommPortIdentifier portId = null;
	/** The port we're normally going to use. */
	private static final String PORT_NAMES[] = { 
			"/dev/tty.usbserial-A9007UX1", // Mac OS X
			"/dev/ttyACM0", // Raspberry Pi
			"/dev/ttyUSB0", // Linux
			"COM6", // Windows
	};

	public void initialize() {

		
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();

		//First, Find an instance of serial port as set in PORT_NAMES.
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			for (String portName : PORT_NAMES) {
				if (currPortId.getName().equals(portName)) {
					portId = currPortId;
					break;
				}
			}
		}

		if (portId == null) {
			System.out.println("Could not find COM port.");
			return;
		}

		try {
			// open serial port, and use class name for the appName.
			serialPort = (SerialPort) portId.open(this.getClass().getName(),
					TIME_OUT);

			// set port parameters
			serialPort.setSerialPortParams(DATA_RATE,
					SerialPort.DATABITS_8,
					SerialPort.STOPBITS_1,
					SerialPort.PARITY_NONE);

			// open the streams
			input = new BufferedReader(new InputStreamReader(serialPort.getInputStream()));
			output =serialPort.getOutputStream();

			// add event listeners
			serialPort.addEventListener(this);
			serialPort.notifyOnDataAvailable(true);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
	}

	public synchronized void close() {
		if (serialPort != null) {
			serialPort.removeEventListener();
			serialPort.close();
		}
	}

	public synchronized void serialEvent(SerialPortEvent oEvent) {
		if (oEvent.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			try {

				String inputLine=input.readLine();

				System.out.println(inputLine);



			} catch (Exception e) {
				System.err.println(e.toString());
			}
		}
		// Ignore all the other eventTypes, but you should consider the other ones.
	}

	Thread t = new Thread() {
		public void run() {
			String line;
			BufferedReader bis;
			while(true) {
				try {
					bis = new BufferedReader(new InputStreamReader(url.openStream()));
					while( (line = bis.readLine()) != null) {
						if(line.equals("on")) {
							if(currentState.equals("off")) {
								System.out.println("on state");
								currentState = "on";
								readWrite("on");
							}
						} else if(line.equals("off")) {
							if(currentState.equals("on")) {
								System.out.println("off state");
								currentState = "off";
								readWrite("off");
							}								
						}
					}
					Thread.sleep(2000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

	};

	public void readWrite(String sw) {
		String data = "y" ;

		if(sw.equals("on")) {
			data = "a";
		}
		else {
			data = "y";
		}

		try{
			output.write(data.getBytes());
			output.flush();
			output.close();
		} catch (Exception e) {
			System.out.println(e);
		}


	}

	public static void main(String[] args) throws Exception
	{
		Sw sw = new Sw();
		url=  new URL("http://www.alokpanda.com/a/data");
		sw.initialize();
		Thread.sleep(1000);
		if(sw.portId != null) {
			sw.t.start();
		}

	}
}
